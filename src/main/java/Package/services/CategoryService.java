package Package.services;

import Package.models.Category;
import Package.repo.DBMainRepo;

import java.util.List;

public class CategoryService {

    public List<Category> load() {
        return DBMainRepo.getCategories();
    }
}
