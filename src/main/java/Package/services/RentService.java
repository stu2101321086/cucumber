package Package.services;


import Package.repo.DBMainRepo;
import Package.models.Book;
import Package.models.Rent;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class RentService
{
    public String rentBook(String bookName,
                               LocalDate startDate,
                               LocalDate endDate,
                               String comment)
    {
        String errorMessage = isInputsValid(startDate, endDate, comment);
        if (errorMessage != null)
        {
            return errorMessage;
        }
        Book kniga = DBMainRepo.getBooks()
                .stream()
                .filter(book -> book.getTitle().equals(bookName))
                .findFirst()
                .orElse(null);
        if (kniga == null){
            return "Книгата не беше намерена";
        }
        if (!isBookAvailable(kniga, startDate, endDate))
        {
            return "Книгата е заета за избраният период";
        }
        kniga.getRentSet().add(new Rent(1L, startDate, endDate, comment));
        return generateMessage(bookName, startDate, endDate, kniga);
    }


    private static String isInputsValid(LocalDate startDate, LocalDate endDate, String comment)
    {
        if (Objects.isNull(startDate) && Objects.isNull(endDate))
        {
            return "Изберете период за наема.";
        }
        if (comment != null && comment.length() > 50)
        {
            return "Коментара е прекалено дълъг -до 50с.";
        }
        if (comment != null && !comment.isBlank() && !comment.matches("[\\w\\d\\.,:!\\p{L}\\s-]+"))
        {
            return "Коментара може да съдържа само текст, числа и следните синволи: ,.!-";
        }
        return null;
    }


    private boolean isBookAvailable(Book kniga, LocalDate startDate, LocalDate endDate)
    {
        return kniga.getRentSet()
                .stream()
                .noneMatch(rent -> rent.getStartDate().isBefore(endDate) && rent.getEndDate().isAfter(startDate));
    }


    private String generateMessage(String bookName, LocalDate startDate, LocalDate endDate, Book kniga)
    {
        StringBuilder message = new StringBuilder("Успешно наехте ");
        message.append(bookName);
        message.append(" за периода ");
        message.append(startDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        message.append("-");
        message.append(endDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        message.append(" на обща стойност ");
        message.append(kniga.getPrice() * (endDate.toEpochDay() - startDate.toEpochDay()));
        message.append("лв.");
        return message.toString();
    }
}
