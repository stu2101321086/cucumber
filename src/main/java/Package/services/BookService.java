package Package.services;

import Package.models.Category;
import Package.models.Book;
import Package.repo.DBMainRepo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BookService {


    public List<Book> load(String categoryName, String bookName) {

        List<Book> bookList = DBMainRepo.getBookList();
        List<Book> result = new ArrayList<>();
        result = filterByCategory(categoryName, result, bookList);
        result = filterByKnigaName(bookName, result);
        return result;
    }

    private static List<Book> filterByKnigaName(String bookName, List<Book> result) {
        if (bookName != null && !bookName.trim().isEmpty()) {
            result =  result.stream().filter(b -> b.getTitle().contains(bookName)).collect(Collectors.toList());
        }
        return result;
    }

    private static List<Book> filterByCategory(String categoryName, List<Book> result, List<Book> bookList) {
        if (categoryName == null || categoryName.trim().isEmpty()) {
            result = bookList;
        } else {
            /*return bookList.stream()
                    .filter(b -> b.getCategories()
                            .stream()
                            .anyMatch(c -> c.getTitle().equals(category)))
                    .collect(Collectors.toList());*/
            for (Book book : bookList) {
                for (Category category : book.getCategories()) {
                    if (category.getTitle().equals(categoryName)) {
                        result.add(book);
                        break;
                    }
                }
            }
        }
        return result;
    }
}
