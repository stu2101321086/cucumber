package Package.repo;


import lombok.Getter;
import Package.models.Category;
import Package.models.Book;
import Package.models.Rent;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.time.LocalDate;
import java.util.HashSet;

@Getter
public class DBMainRepo {
    private static List<Book> bookList = new ArrayList<>();
    private static List<Category> categories = new ArrayList<>();
    private static List<Book> books = new ArrayList<>();
    private static List<Rent> rents = new ArrayList<>();

    static
    {
        Category category1 = new Category(1l, "Adventure", "Prikliu4enie", null, null);
        Category category2 = new Category(2l, "Math", "Matematika", null, null);
        Category category3 = new Category(3l, "History", "HIS STORY", null, null);

        books.add(new Book(1l, "around the world in 80 days", "", 50, 12.82d, Set.of(category1), new HashSet<>()));
        books.add(new Book(2l, "Jumanji", "", 50, 12.29d, Set.of(category1), new HashSet<>()));
        Book kniga2 = new Book(3l, "Matrix", "", 50, 3.01d, Set.of(category1), new HashSet<>());

        books.add(kniga2);

        HashSet<Rent> rentSet = new HashSet<>();
        rentSet.add(new Rent(1L, LocalDate.of(2024, 5, 15),
                LocalDate.of(2024, 5, 18), "Note 1"));
        kniga2.setRentSet(rentSet);
    }


    static {
        Category category1 = new Category(1l, "Adventure", "Prikliu4enie", null, null);
        Category category2 = new Category(2l, "Math", "Matematika", null, null);
        Category category3 = new Category(3l, "History", "HIS STORY", null, null);

        bookList.add(new Book(1l, "around the world in 80 days", "", 50, 12.82d, Set.of(category1), Set.of()));
        bookList.add(new Book(2l, "Jumanji", "", 50, 12.29d, Set.of(category1), Set.of()));
        bookList.add(new Book(3l, "Matrix", "", 50, 3.01d, Set.of(category1), Set.of()));
        bookList.add(new Book(4l, "Percy Jackson and the Olympians", "", 50, 7.5d, Set.of(category1), Set.of()));
        bookList.add(new Book(5l, "Odyssey", "", 50, 12.67d, Set.of(category1), Set.of()));
        bookList.add(new Book(6l, "lineina algerbra", "", 50, 19.99d, Set.of(category2), Set.of()));

        categories.add(category1);
        categories.add(category2);
        categories.add(category3);
    }

    public static List<Category> getCategories() {
        return categories;
    }

    public static List<Book> getBookList() {
        return bookList;
    }

    public static List<Book> getBooks()
    {
        return books;
    }


    public static List<Rent> getRents()
    {
        return rents;
    }

}
