package Package.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import java.util.HashSet;
import java.util.Set;
@Setter
@Getter
@AllArgsConstructor
public class Book {
    private Long id;
    private String title;
    private String description;
    private int quantity;
    private double price;
    private Set<Category> categories;
    private Set<Rent> rentSet = new HashSet<>();
    public Set<Rent> getRentSet()
    {
        return rentSet;
    }
}
