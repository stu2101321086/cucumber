package Package.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Objects;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Rent
{
    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;
    private String note;


    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rent rent = (Rent)o;
        return Objects.equals(id, rent.id);
    }


    @Override
    public int hashCode()
    {
        return Objects.hashCode(id);
    }
}