package Package.book;

import lombok.Getter;
import lombok.Setter;
import Package.models.Book;

import java.util.List;
@Getter
@Setter
public class BookHelperModel {
    private String category;
    private String bookName;
    private List<Book> bookList;

}

