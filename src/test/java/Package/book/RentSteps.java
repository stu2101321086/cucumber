package Package.book;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import Package.services.RentService;
import Package.services.BookService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;


public class RentSteps
{
    private final BookHelperModel helperModel;
    private LocalDate startDate;
    private LocalDate endDate;
    private String comment;
    private String message;


    public RentSteps(BookHelperModel helperModel)
    {
        this.helperModel = helperModel;
    }


    @When("въведе начална дата {string}")
    public void addStartDate(String startDateString)
    {
        this.startDate = getParse(startDateString);
    }


    @When("въведе крайна дата {string}")
    public void addEndDate(String endDateString)
    {
        this.endDate = getParse(endDateString);
    }


    private static LocalDate getParse(String endDateString)
    {
        return LocalDate.parse(endDateString, DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }


    @When("въведе коментар {string}")
    public void addComment(String comment)
    {
        this.comment = comment;
    }


    @When("натисне бутона за наем")
    public void submitRent()
    {
        RentService rentService = new RentService();
        message = rentService.rentBook(helperModel.getBookName(), startDate, endDate, comment);
    }


    @Then("вижда съобщение {string}")
    public void checkMessage(String expectedMessage)
    {
        assertEquals(expectedMessage, message);
    }
}