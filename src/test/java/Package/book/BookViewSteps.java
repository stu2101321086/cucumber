package Package.book;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Given;
import Package.models.Category;
import Package.services.CategoryService;
import Package.services.BookService;
import Package.services.RentService;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BookViewSteps {
    private final BookHelperModel helperModel;

    public BookViewSteps(BookHelperModel helperModel) {
        this.helperModel = helperModel;
    }

    @When("отваряне на приложението")
    public void openApp() {
        //missing real app
    }

    @When("натиска бутона за търсене")
    public void clickSearchButton() {
        BookService bookService = new BookService();
        helperModel.setBookList(bookService.load(helperModel.getCategory(),
                helperModel.getBookName()));
    }


    @Then("Визуализира се списък с категории")
    public void checkCategories() {
        CategoryService categoryService = new CategoryService();
        List<Category> categories = categoryService.load();
        assertTrue(categories.stream().anyMatch(c -> c.getTitle().equals("Adventure")));
        assertTrue(categories.stream().anyMatch(c -> c.getTitle().equals("Math")));
        assertTrue(categories.stream().anyMatch(c -> c.getTitle().equals("History")));
        assertEquals(3, categories.size());
    }

    @When("потребителя избере категория: {string}")
    public void chooseCategory(String category) {
        this.helperModel.setCategory(category);
    }


    @When("потребителя въвежда име на книга {string} в полето за търсене")
    public void addBookInSearchField(String bookName) {
        this.helperModel.setBookName(bookName);
    }

    @Given("Потребителя отваря детайлите на {string}")
    public void openBookDetails(String bookName)
    {
        helperModel.setBookName(bookName);
    }
}

